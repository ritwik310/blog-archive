**BitTorrent** is a **peer-to-peer** (P2P) file sharing protocol over the Internet, where a computer can join a swarm of other computers to exchange **pieces of data** between each other. Each computer that is connected to the network is called a **Peer**. And, each **peer** is connected to **multiple peers at the same time**, and thus downloading or uploading to multiple peers at the same time. Unlike normal downloads, a torrent download doesn't put any signoficant load on a single peer's bandwidth, so it's faster.

# What this post is for?

In this tutorial we are gonna write a **BitTorrent-Client** from scratch in **Go**. Our focus will be downloading files from the peers. We are not gonna focus on file sharing for now. By the end, you can download files via the BitTorrent-protocol from the command-line.

# Where's the "tutorial"?

<div>
  <p>
    <span style="font-size: 30px;">
      &ltbig&gtComing soon!&lt/big&gt
    </span>
    <br>
    Within a couple of weeks max (in Elon's time scale), today is Sep-13-2019</span>
  </p>
</div>