A subset (recency bias) of things that I have found valuable over the years on the Internet (A never completing list).

# Inspirational Video & Articles

You Can Learn Anything, Khan Academy - [youtube.com](https://youtu.be/JC82Il2cjqA)   

Steve Jobs' 2005 Stanford Commencement Address - [youtube.com](https://youtu.be/UF8uR6Z6KLc)   
What You'll Wish You'd Known, by Paul Graham - [paulgraham.com](http://www.paulgraham.com/hs.html)   

A Pale Blue Dot, by Carl Sagan - [youtube.com](https://youtu.be/EWPFmdAWRZ0)   

Gods and Worms - [youtube.com](https://youtu.be/mJKxxaCmJLo)   
Rick and Morty and the Meaning of Life - [hackernoon.com](https://hackernoon.com/rick-and-morty-and-the-meaning-of-life-6640df17e263)  

The Egg, A Short Story, from Kurzgesagt - [youtube.com](https://youtu.be/h6fcK_fRYaI)   
The Beginning & End of Humanity - [youtube.com](https://youtu.be/uHu8fEIViA8)   

Why You Can Change The World, But Shouldn't - [youtube.com](https://youtu.be/7Y1EDxsKjrc)   
The Backwards Law, Why Happiness Is Ruining Your Life - [youtube.com](https://youtu.be/hPhc9FU7ycI)   

Instructions to a Good Life - [youtube.com](https://youtu.be/A4pR--qJTdU)   
Oh Hello, You're Alive - [youtube.com](https://youtu.be/VLAAy_pM-k8)   
The Last Thought You'll Ever Have - [youtube.com](https://youtu.be/CEpFVz5vVVM)   

Dictionary of Obscure Sorrows - [youtube.com](https://www.youtube.com/playlist?list=PLCXinA-ukN0rxft8oNal97bH3FfRzSZXZ)

The Humans of New York, Instagram - [instagram.com](https://www.instagram.com/humansofny)   

# Interesting Resources

Two Minutes to Midnight, Aperture - [youtube.com](https://youtu.be/bsZ37kxrQYg)   
The Artificial Intelligence That Deleted A Century, by Tom Scott - [youtube.com](https://youtu.be/-JlxuQ7tPgQ)   

Jailbreaking the Simulation by George Hotz - [youtube.com](https://youtu.be/ESXOAJRdcwQ)   

Why Boredom is Good For You, by Veritasium - [youtube.com](https://www.youtube.com/watch?v=LKPwKFigF8U)   
Thinking about Attention, by CGP Grey - [youtube.com](https://youtu.be/wf2VxeIm1no)   
How to Get Your Life Back Together, by Improvement Pill - [youtube.com](https://youtu.be/vl-44jDYDJQ)

You, Version Two, from Freedom in Thought - [youtube.com](https://www.youtube.com/playlist?list=PL5faAYlGYYoGwdnSOCC2uOeJM4_cnjEME)

History of the Entire World, by Bill Wurtz - [youtube.com](https://youtu.be/xuCn8ux2gbs)   
Evolution (Mathematics of Evolution), by Justin Helps - [youtube.com](https://www.youtube.com/playlist?list=PLKortajF2dPBWMIS6KF4RLtQiG6KQrTdB)   
Memes & The Evolution of Art, from Pursuit of Wonder - [youtube.com](https://youtu.be/vWaId-BvIrA)   

This is the Scariest Graph, by Hank Green - [youtube.com](https://youtu.be/Ix2XprA6A7I)   

Steve Jobs, Think Different (Best Marketing Strategy) - [youtube.com](https://youtu.be/keCwRdbwNQY)   

The Greatest Story Ever Told, Part 1 - [youtube.com](https://youtu.be/A2wG0sXbMhw)   
The Greatest Story Ever Told, Part 2 - [youtube.com](https://youtu.be/-JOXUNX66Cs)   

Naval Ravikant's Archive (thoughts on Startups, Investing and Life) - [theangelphilosopher.com](https://theangelphilosopher.com) and [nav.al](http://nav.al)   

An Interview with Sam Altman, on Entrepreneurship (the best I've ever watched) - [youtube.com](https://youtu.be/NAaRhXQCt9o)   

The Bus Ticket Theory of Genius, by Paul Graham - [paulgraham.com](http://paulgraham.com/genius.html)   
Paul Graham's Essays - [paulgraham.com](http://www.paulgraham.com/articles.html)   

Artificial Intelligence Podcast, by Lex Fridman - [youtube.com](https://www.youtube.com/playlist?list=PLrAXtmErZgOdP_8GztsuKi9nrraNbKKp4)   

# Interesting Resources (Technical)

Building an 8-bit Breadboard Computer by Ben Eater - [youtube.com](https://www.youtube.com/playlist?list=PLowKtXNTBypGqImE405J2565dvjafglHU)   
Neural Network, by 3Blue1Brown - [youtube.com](https://www.youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)

# Free Learning Platforms on The Internet

Google Searchbar - [google.com](https://google.com)   
Youtube Searchbar - [youtube.com](https://www.youtube.com)   
Wikipedia - [wikipedia.org](https://www.wikipedia.org/)

Khan Academy - [khanacademy.org](https://www.khanacademy.org/)   
MIT OpenCourseWare - [ocw.mit.edu](https://ocw.mit.edu)   
Edx - [edx.org](https://www.edx.org/)   
Coursera - [coursera.org](https://www.coursera.org/)   
<!--Udacity Free Courses - [https://udacity.com/courses/all](https://udacity.com/courses/all)   -->
<!--Stanford Online - [https://online.stanford.edu](https://online.stanford.edu/)   -->

# Free Learning Resources (Business & Startup)

Startup Funding Explained, in 10 minutes - [youtube.com](https://www.youtube.com/watch?v=677ZtSMr4-4)

Startup Playbook by Sam Altman - [playbook.samaltman.com](https://playbook.samaltman.com/)   
How to Start a Startup, Y Combinator at Stanford, 2015 - [youtube.com](https://www.youtube.com/playlist?list=PL5q_lef6zVkaTY_cT1k7qFNF2TidHCe-1)   

How to Get Rich (without getting lucky), by Naval Ravikant - [youtube.com](https://www.youtube.com/playlist?list=PL5QAQBV5zCqVsMqk5UH-LVYuNmM4i6pgW)   
How to Get Rich (the tweetstorm), by Naval Ravikant - [twitter.com](https://twitter.com/naval/status/1002103360646823936)   

# Free Learning Resources (Computer-Science & Programming)

Computer Science, Self-Study Roadmap - [github.com](https://github.com/ossu/computer-science)   
List of Amazing Computer-Science Research Papers - [github.com](https://github.com/papers-we-love/papers-we-love)   
Master Command Line, in 1 Page - [github.com](https://github.com/jlevy/the-art-of-command-line)   

Operating Systems, Three Easy Pieces (free book) - [pages.cs.wisc.edu](http://pages.cs.wisc.edu/~remzi/OSTEP/)   

# Free Learning Resources (Physics)

So you want to learn physics, by Susan Fowler - [susanjfowler.com](https://www.susanjfowler.com/blog/2016/8/13/so-you-want-to-learn-physics)   
The Theoretical Minimum Lectures, by Leonard Susskind - [theoreticalminimum.com](https://theoreticalminimum.com/courses)   

# Free Learning Resources (Mathematics)

3Blue1Brown, Fall in Love with Math - [youtube.com](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw)   
A List of Free Learning Resources - [github.com](https://github.com/rossant/awesome-math)   
