
# Life & Philosophy

Steve Jobs' 2005 Stanford Commencement Address - [youtube.com](https://youtu.be/UF8uR6Z6KLc)  

You Can Learn Anything, Khan Academy - [youtube.com](https://youtu.be/JC82Il2cjqA)

A Pale Blue Dot, by Carl Sagan - [youtube.com](https://youtu.be/EWPFmdAWRZ0) 

Gods and Worms - [youtube.com](https://youtu.be/mJKxxaCmJLo)  

Rick and Morty and the Meaning of Life - [hackernoon.com](https://hackernoon.com/rick-and-morty-and-the-meaning-of-life-6640df17e263)  

The Egg, A Short Story, from Kurzgesagt - [youtube.com](https://youtu.be/h6fcK_fRYaI)   
The Beginning & End of Humanity - [youtube.com](https://youtu.be/uHu8fEIViA8)  

Why You Can Change The World, But Shouldn't - [youtube.com](https://youtu.be/7Y1EDxsKjrc)   

The Backwards Law, Why Happiness Is Ruining Your Life - [youtube.com](https://youtu.be/hPhc9FU7ycI)   

Instructions to a Good Life - [youtube.com](https://youtu.be/A4pR--qJTdU)   
Oh Hello, You're Alive - [youtube.com](https://youtu.be/VLAAy_pM-k8)   
The Last Thought You'll Ever Have - [youtube.com](https://youtu.be/CEpFVz5vVVM)  

You Can Learn Anything, Khan Academy - [youtube.com](https://youtu.be/JC82Il2cjqA)   

Dictionary of Obscure Sorrows - [youtube.com](https://www.youtube.com/playlist?list=PLCXinA-ukN0rxft8oNal97bH3FfRzSZXZ)

The Humans of New York, Instagram - [instagram.com](https://www.instagram.com/humansofny) 

# Science

# Interesting Science Videos

# Learning Resources

# No Categories

Two Minutes to Midnight, Aperture - [youtube.com](https://youtu.be/bsZ37kxrQYg)   

The Artificial Intelligence That Deleted A Century, by Tom Scott - [youtube.com](https://youtu.be/-JlxuQ7tPgQ)  

Why Boredom is Good For You, by Veritasium - [youtube.com](https://www.youtube.com/watch?v=LKPwKFigF8U)   
Thinking about Attention, by CGP Grey - [youtube.com](https://youtu.be/wf2VxeIm1no)   
How to Get Your Life Back Together, by Improvement Pill - [youtube.com](https://youtu.be/vl-44jDYDJQ)

You, Version Two, from Freedom in Thought - [youtube.com](https://www.youtube.com/playlist?list=PL5faAYlGYYoGwdnSOCC2uOeJM4_cnjEME)

History of the Entire World, by Bill Wurtz - [youtube.com](https://youtu.be/xuCn8ux2gbs) 

Evolution (Mathematics of Evolution), by Justin Helps - [youtube.com](https://www.youtube.com/playlist?list=PLKortajF2dPBWMIS6KF4RLtQiG6KQrTdB) 

Memes & The Evolution of Art, from Pursuit of Wonder - [youtube.com](https://youtu.be/vWaId-BvIrA)   

This is the Scariest Graph, by Hank Green - [youtube.com](https://youtu.be/Ix2XprA6A7I)   

The Tesla Truck and Environmentalism, by Hank Green - [youtube.com](https://youtu.be/rFh5-eIw_cs)


Systems, by John Green - [youtube.com](https://youtu.be/InSvBuHK4vI)

The Greatest Story Ever Told, Part 1 - [youtube.com](https://youtu.be/A2wG0sXbMhw)   
The Greatest Story Ever Told, Part 2 - [youtube.com](https://youtu.be/-JOXUNX66Cs)   

