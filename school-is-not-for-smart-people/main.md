This article is just a aggrigation of a recent tweet strom of mine, titled "Why school is not a place for smart people?".

<div>
  <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Schools are less about learning and more about proving how much you&#39;ve learned.</p>&mdash; Ritwik (@ritsource) <a href="https://twitter.com/ritsource/status/1194884539816062976?ref_src=twsrc%5Etfw">November 14, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>

If you want to be average school is the most efficient option. If you want to be smart you need to find other arrangements (parallel or non-parallel).

Education is about learning, schools are about winning - and that's where school misses out on its very purpose, which is education. A system that is focused more on the elimination of participants, than the distribution of services is not at all in everyone's best interest.

Education is the process of facilitating learning, or the acquisition of knowledge, skills, values, beliefs, and habits. Educational methods include storytelling, discussion, teaching, training, and directed research. (Wikipedia)

There's nothing like competitions, exams or "passing thresholds" in the definition above. Cause those are not at all related to education, it's just a medium of motivation for people who go through the system, meant to educate in mass.

Everything that humans do is somehow related to a reward system. Even there's a reward system responsible for our eating. exams, competitions, and status are just rewards that we are introduced to in order to take study seriously.

But, most of us are couldn't see the negative side of exams. Exams are the biggest vulnerability of the education system (especially in India).

Most exams are just a process for mechanical copy-paste. It's mostly just a test of human labor, not intelligence, neither creativity. It rewards repetitive study that doesn't have an element of problem-solving or creativity, over one that has.

How is it that being good is art studies is not at all related to a student's creativity? And, one doesn't even need to understand how 0, 1, 2, 3, +, -, %, x... translates to calculus and what complex-numbers are in order to get an A+ in the math exam.

Not only that, most of the things that students study in school, are forgotten after exams. What economic, scientific or moral value does memorization bring to one's life? None.

Human brains are not at all designed to store unrelatable data. Investing your energy and time to remember facts that you can write in exams (and probably forget after a while), is just a waste of time, energy, and entropy. That's not in the best interests of smart people.

The purpose of education needs to be changed from that of producing a literate society to that of producing a learning society.

Not at the system level, but at the awareness level.

Where learning becomes passion, not durdgery.